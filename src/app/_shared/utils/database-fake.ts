import { Action } from '../../_models/action';
import { Strategy } from '../../_models/strategy';
import { System } from '../../_models/system';

export const SYSTEMS: System[] = [
    { id: 1, name: 'SNOA', description: 'Sistema de Negociação de Oferta e Atacado', strategy: 'by Id' },
    { id: 2, name: 'BDA', description: 'Base de Dados e Atacado', strategy: 'by Xpath' },
    { id: 3, name: 'SVA', description: 'Sistema de Valor Agregado', strategy: 'Contém Nome' }
];


export const STRATEGIES: Strategy[] = [
    { id: 1, name: 'by Id', strategyDefault: false, codeGroovy: 'public class HelloWord(){}', system: SYSTEMS[ 0 ] },
    { id: 2, name: 'by Xpath', strategyDefault: true, codeGroovy: 'public class HelloWord(){}', system: SYSTEMS[ 1 ] },
    { id: 3, name: 'Contém Nome', strategyDefault: false, codeGroovy: 'public class HelloWord(){}', system: SYSTEMS[ 2 ] }
];

export const ACTIONS: Action[] = [
    { id: 1, name: 'Click + Input', actionDefault: false, codeGroovy: 'public class HelloWord(){}', system: SYSTEMS[ 0 ] },
    { id: 2, name: 'Verifica Texto Igual', actionDefault: true, codeGroovy: 'public class HelloWord(){}', system: SYSTEMS[ 1 ] },
    { id: 3, name: 'Verifica Text Contém', actionDefault: false, codeGroovy: 'public class HelloWord(){}', system: SYSTEMS[ 2 ] }
];
