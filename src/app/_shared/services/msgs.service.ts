import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Message } from 'primeng/components/common/api';

type Severities = 'success' | 'info' | 'warn' | 'error';

@Injectable()
export class MsgsService {

    notificationGrowlChange: Subject<Object> = new Subject<Object>();
    notificationMsgChange: Subject<Object> = new Subject<Object>();
    notificationMsgChangeAll: Subject<Object> = new Subject<Object>();

    constructor() { }

    notifyMessage( severity: Severities, summary: string, detail: string ) {
        this.notificationMsgChange.next( { severity, summary, detail } );
    }

    notifyAllMessage( errorsList: Message[] ) {
        this.notificationMsgChangeAll.next( errorsList );
    }

    notifyGrowl( severity: Severities, summary: string, detail: string, isSticky?: boolean, lifeTime?: number ) {
        this.notificationGrowlChange.next( { severity, summary, detail, isSticky, lifeTime } );
    }

    notifyGrowlRegister( entity: string ) {
        this.notifyGrowl( 'success', null, `${ entity } cadastrado com sucesso.` );
    }

    notifyGrowlEdit( entity: string ) {
        this.notifyGrowl( 'success', null, `${ entity } editado com sucesso.` );
    }

    notifyGrowlRemove( entity: string ) {
        this.notifyGrowl( 'success', null, `${ entity } removido com sucesso.` );
    }

    clear() {
        this.notificationMsgChange.next( null );
        this.notificationMsgChangeAll.next( [] );
    }
}
