import { AllValidationErrors, getFormValidationErrors } from '../utils/validate-form-errors';
import { MsgsService } from './msgs.service';
import { Injectable, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Message } from 'primeng/components/common/api';

@Injectable()
export class ValidateFormService {

    form: FormGroup;
    formSumitAttempt: boolean;

    constructor( private msgsService: MsgsService ) {
    }

    isFieldValid( field: string ) {
        if ( !this.form.get( field ) ) {
            console.error( 'Field dont exists in form: ' + field );
        }
        return (
            ( !this.form.get( field ).valid && this.form.get( field ).touched ) ||
            ( this.form.get( field ).untouched && this.formSumitAttempt )
        );
    }
    displayFieldCss( field: string ) {
        return {
            'ng-dirty': this.isFieldValid( field )
        };
    }

    //    validateAllFormFields( formGroup: FormGroup ) {
    //        Object.keys( formGroup.controls ).forEach( field => {
    //            const control = formGroup.get( field );
    //            if ( control instanceof FormControl ) {
    //                control.markAsTouched( { onlySelf: true } );
    //            } else if ( control instanceof FormGroup ) {
    //                this.validateAllFormFields( control );
    //            }
    //        } );
    //    }

    displayFieldErrors() {
        this.formSumitAttempt = true;
        const errors = getFormValidationErrors( this.form.controls );
        const errorsList: Message[] = [];

        errors.forEach( ( error: AllValidationErrors ) => {
            if ( error ) {
                let text;
                switch ( error.error_name ) {
                    case 'required': text = `${ error.control_name } é obrigatório.`; break;
                    case 'pattern': text = `${ error.control_name } com padrão de formato inválido.`; break;
                    case 'email': text = `${ error.control_name } com formato de e-mail inválido.`; break;
                    case 'minlength': text = `${ error.control_name } com tamanho inválido. Tamanho necessário: ${ error.error_value.requiredLength }`; break;
                    case 'areEqual': text = `${ error.control_name } deve ser igual.`; break;
                    default: text = `${ error.control_name }: ${ error.error_name }: ${ error.error_value }`;
                }

                errorsList.push( { severity: 'error', summary: '', detail: text } );
            }
        } );
        this.msgsService.notifyAllMessage( errorsList );
        //        this.msgsService.notify( 'error', null, text );
    }

    clear() {
        this.form.reset();
        this.formSumitAttempt = false;
        this.msgsService.clear();
    }

}
