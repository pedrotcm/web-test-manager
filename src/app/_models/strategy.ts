import { System } from './system';
export class Strategy {
    id: number;
    name: string;
    strategyDefault: boolean;
    codeGroovy: string;
    system: System;
}
