export class System {
    id: number;
    name: string;
    strategy: string;
    description: string;
}
