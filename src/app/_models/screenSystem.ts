import { System } from './system';
export class ScreenSystem {
    id: number;
    name: string;
    dependsOnLogin: boolean;
    isLoginScreen: boolean;
    description: string;
    pathUrl: string;
    system: System;
    testUrl: string;
}
