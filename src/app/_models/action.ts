import { System } from './system';
export class Action {
    id: number;
    name: string;
    actionDefault: boolean;
    codeGroovy: string;
    system: System;
}
