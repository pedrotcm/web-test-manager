import { ActionComponent } from './action.component';
import { ConsultActionComponent } from './consult/consult-action.component';
import { RegisterActionComponent } from './register/register-action.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '', component: ActionComponent,
        children: [
            { path: 'register', component: RegisterActionComponent },
            { path: 'consult', component: ConsultActionComponent },

        ]
    }
];

@NgModule( {
    imports: [ RouterModule.forChild( routes ) ],
    exports: [ RouterModule ]
} )
export class ActionRoutingModule { }
