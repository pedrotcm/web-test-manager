import { Action } from '../../../_models/action';
import { MsgsService } from '../../../_shared/services/msgs.service';
import { ValidateFormService } from '../../../_shared/services/validate-form.service';
import { ACTIONS } from '../../../_shared/utils/database-fake';
import { routerTransition } from '../../../router.animations';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ConfirmationService } from 'primeng/api';

@Component( {
    selector: 'app-consult-action',
    templateUrl: './consult-action.component.html',
    styleUrls: [ './consult-action.component.scss' ],
    animations: [ routerTransition ],
    providers: [ ConfirmationService, ValidateFormService ]
} )
export class ConsultActionComponent implements OnInit {

    index = 0;
    consultForm: FormGroup;
    actions: Action[];
    cols: any[];
    loading: boolean;
    editTab = false;

    entityEditSelected: Action;

    constructor(
        private fb: FormBuilder,
        public validateFormService: ValidateFormService,
        private confirmationService: ConfirmationService,
        private msgsService: MsgsService
    ) { }

    ngOnInit() {
        this.consultForm = this.fb.group( {
            'name': new FormControl(),
        } );
        this.validateFormService.form = this.consultForm;

        this.cols = [
            { field: 'name', header: 'Nome' },
            { field: 'system', header: 'Sistema' },
            { field: 'actionDefault', header: 'Ação Padrão' }
        ];

        this.actions = ACTIONS;

    }

    receiveEditMessage( $event ) {
        if ( $event ) {
            this.index = 0;
            this.editTab = false;
            this.onSubmit( this.consultForm.value );
        }
    }

    onSubmit( value: string ) {
        console.log( JSON.stringify( value ) );
    }

    handleClose( e ) {
        this.index = 0;
        this.editTab = false;
        e.close();
    }

    handleChange( e ) {
        this.index = e.index;
    }

    edit( editEntity: Action ) {
        this.index = 1;
        this.editTab = true;
        this.entityEditSelected = editEntity;
    }

    remove( system: Action ) {
        this.confirmationService.confirm( {
            header: 'Confirmar exclusão',
            message: 'Deseja realmente excluir este registro?',
            icon: 'fa fa-trash',
            accept: () => {
                this.msgsService.notifyGrowlRemove( 'Ação' );
            },
            reject: () => {
            }
        } );
    }
}
