import { Action } from '../../../_models/action';
import { MsgsService } from '../../../_shared/services/msgs.service';
import { ValidateFormService } from '../../../_shared/services/validate-form.service';
import { SYSTEMS } from '../../../_shared/utils/database-fake';
import { routerTransition } from '../../../router.animations';
import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component( {
    selector: 'app-register-action',
    templateUrl: './register-action.component.html',
    styleUrls: [ './register-action.component.scss' ],
    animations: [ routerTransition ],
    providers: [ ValidateFormService ]
} )
export class RegisterActionComponent implements OnInit, OnDestroy {

    registerForm: FormGroup;
    filteredSystems: any[];
    systems = SYSTEMS;

    editorOptions = { theme: 'vs-dark', language: 'java' };
    code: string;

    @Input()
    entityEdit: Action;

    @Output()
    editMessageEvent = new EventEmitter<string>();

    constructor(
        private fb: FormBuilder,
        public validateFormService: ValidateFormService,
        private msgsService: MsgsService
    ) { }

    ngOnInit() {
        this.registerForm = this.fb.group( {
            'id': new FormControl(),
            'name': new FormControl( null, Validators.required ),
            'actionDefault': new FormControl( false ),
            'codeGroovy': new FormControl( null, Validators.required ),
            'system': new FormControl( null, Validators.required )
        } );

        if ( this.entityEdit ) {
            this.registerForm.setValue( this.entityEdit );
        }
        this.validateFormService.form = this.registerForm;
    }
    ngOnDestroy() {
        this.validateFormService.clear();
    }

    onSubmit( { value, valid }: { value: Action, valid: boolean } ) {
        if ( valid ) {
            console.log( value );
            if ( this.entityEdit ) {
                this.msgsService.notifyGrowlEdit( 'Ação' );
                this.editMessageEvent.emit( 'ok' );
            } else {
                this.msgsService.notifyGrowlRegister( 'Ação' );
            }
            this.validateFormService.clear();
        } else {
            this.validateFormService.displayFieldErrors();
        }
    }

    onChangeAction( checked ) {
        if ( checked ) {
            this.registerForm.controls[ 'system' ].clearValidators();
            this.registerForm.controls[ 'system' ].updateValueAndValidity();
            this.registerForm.controls[ 'system' ].reset( { value: null, disabled: true } );
        } else {
            this.registerForm.controls[ 'system' ].setValidators( Validators.required );
            this.registerForm.controls[ 'system' ].updateValueAndValidity();
            this.registerForm.controls[ 'system' ].reset( { value: null, disabled: false } );
        }
    }

    filterSystems( event ) {
        this.filteredSystems = [];
        if ( event.query.toLowerCase() === '' ) {
            this.filteredSystems = [];
        }
        for ( let i = 0; i < this.systems.length; i++ ) {
            if ( this.systems[ i ].name.toLowerCase().indexOf( event.query.toLowerCase() ) === 0 ) {
                this.filteredSystems.push( this.systems[ i ] );
            }
        }
    }

    clearAutoComplete( event ) {
        this.registerForm.controls[ 'system' ].reset();
    }
}
