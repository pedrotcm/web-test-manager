import { BreadcrumbModule } from '../../_shared/modules/breadcrumb/breadcrumb.module';
import { ActionRoutingModule } from './action-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterActionComponent } from './register/register-action.component';
import { ConsultActionComponent } from './consult/consult-action.component';
import { ActionComponent } from './action.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { TooltipModule } from 'primeng/tooltip';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { CheckboxModule } from 'primeng/checkbox';
import { MonacoEditorModule } from 'ngx-monaco-editor';

@NgModule( {
    imports: [
        CommonModule,
        ActionRoutingModule,
        BreadcrumbModule,
        FormsModule,
        ReactiveFormsModule,
        InputTextModule,
        ButtonModule,
        TableModule,
        TabViewModule,
        TooltipModule,
        ConfirmDialogModule,
        AutoCompleteModule,
        CheckboxModule,
        MonacoEditorModule.forRoot()
    ],
    declarations: [ ActionComponent, RegisterActionComponent, ConsultActionComponent ]
} )
export class ActionModule { }
