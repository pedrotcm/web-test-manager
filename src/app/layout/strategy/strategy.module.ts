import { BreadcrumbModule } from '../../_shared/modules/breadcrumb/breadcrumb.module';
import { MsgsModule } from '../../_shared/modules/messages/msgs.module';
import { ConsultStrategyComponent } from './consult/consult-strategy.component';
import { RegisterStrategyComponent } from './register/register-strategy.component';
import { StrategyRoutingModule } from './strategy-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StrategyComponent } from './strategy.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { TooltipModule } from 'primeng/tooltip';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { CheckboxModule } from 'primeng/checkbox';
import { MonacoEditorModule } from 'ngx-monaco-editor';

@NgModule( {
    imports: [
        CommonModule,
        StrategyRoutingModule,
        BreadcrumbModule,
        FormsModule,
        ReactiveFormsModule,
        InputTextModule,
        ButtonModule,
        TableModule,
        TabViewModule,
        TooltipModule,
        ConfirmDialogModule,
        AutoCompleteModule,
        CheckboxModule,
        MonacoEditorModule.forRoot()
    ],
    declarations: [ StrategyComponent, RegisterStrategyComponent, ConsultStrategyComponent ]
} )
export class StrategyModule { }
