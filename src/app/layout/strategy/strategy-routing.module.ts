import { ConsultStrategyComponent } from './consult/consult-strategy.component';
import { RegisterStrategyComponent } from './register/register-strategy.component';
import { StrategyComponent } from './strategy.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '', component: StrategyComponent,
        children: [
            { path: 'register', component: RegisterStrategyComponent },
            { path: 'consult', component: ConsultStrategyComponent },
        ]
    }
];

@NgModule( {
    imports: [ RouterModule.forChild( routes ) ],
    exports: [ RouterModule ]
} )
export class StrategyRoutingModule { }
