import { BreadcrumbModule } from '../../_shared/modules/breadcrumb/breadcrumb.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';

@NgModule( {
    imports: [
        CommonModule,
        DashboardRoutingModule,
        BreadcrumbModule
    ],
    declarations: [ DashboardComponent ]
} )
export class DashboardModule { }
