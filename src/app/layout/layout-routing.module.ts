import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'system', loadChildren: './system/system.module#SystemModule' },
            { path: 'strategy', loadChildren: './strategy/strategy.module#StrategyModule' },
            { path: 'action', loadChildren: './action/action.module#ActionModule' },
            { path: 'screen', loadChildren: './screen/screen.module#ScreenModule' },

        ]
    }
];

@NgModule( {
    imports: [ RouterModule.forChild( routes ) ],
    exports: [ RouterModule ]
} )
export class LayoutRoutingModule { }
