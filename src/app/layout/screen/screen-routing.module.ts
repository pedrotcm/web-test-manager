import { ConsultScreenComponent } from './consult/consult-screen.component';
import { RegisterScreenComponent } from './register/register-screen.component';
import { ScreenComponent } from './screen.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '', component: ScreenComponent,
        children: [
            { path: 'register', component: RegisterScreenComponent },
            { path: 'consult', component: ConsultScreenComponent },
        ]
    }
];

@NgModule( {
    imports: [ RouterModule.forChild( routes ) ],
    exports: [ RouterModule ]
} )
export class ScreenRoutingModule { }
