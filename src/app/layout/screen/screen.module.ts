import { BreadcrumbModule } from '../../_shared/modules/breadcrumb/breadcrumb.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScreenComponent } from './screen.component';
import { ConsultScreenComponent } from './consult/consult-screen.component';
import { RegisterScreenComponent } from './register/register-screen.component';
import { ScreenRoutingModule } from './screen-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { TooltipModule } from 'primeng/tooltip';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { CheckboxModule } from 'primeng/checkbox';
import { AccordionModule } from 'primeng/accordion';
import { PanelModule } from 'primeng/panel';
import { InplaceModule } from 'primeng/inplace';

@NgModule( {
    imports: [
        CommonModule,
        ScreenRoutingModule,
        BreadcrumbModule,
        FormsModule,
        ReactiveFormsModule,
        InputTextModule,
        ButtonModule,
        TableModule,
        TabViewModule,
        TooltipModule,
        ConfirmDialogModule,
        AutoCompleteModule,
        CheckboxModule,
        AccordionModule,
        PanelModule,
        InplaceModule
    ],
    declarations: [ ScreenComponent, ConsultScreenComponent, RegisterScreenComponent ]
} )
export class ScreenModule { }
