import { ScreenSystem } from '../../../_models/screenSystem';
import { MsgsService } from '../../../_shared/services/msgs.service';
import { ValidateFormService } from '../../../_shared/services/validate-form.service';
import { SYSTEMS, STRATEGIES } from '../../../_shared/utils/database-fake';
import { routerTransition } from '../../../router.animations';
import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';

@Component( {
    selector: 'app-register-screen',
    templateUrl: './register-screen.component.html',
    styleUrls: [ './register-screen.component.scss' ],
    animations: [ routerTransition ],
    providers: [ ValidateFormService ]
} )
export class RegisterScreenComponent implements OnInit, OnDestroy {

    registerForm: FormGroup;
    filteredSystems: any[];
    filteredStrategies: any[];
    systems = SYSTEMS;
    strategies = STRATEGIES;
    cols: any[];
    screenFields: any[];

    @Input()
    entityEdit: ScreenSystem;

    @Output()
    editMessageEvent = new EventEmitter<string>();

    constructor(
        private fb: FormBuilder,
        public validateFormService: ValidateFormService,
        private msgsService: MsgsService
    ) { }

    ngOnInit() {
        this.cols = [
            { field: 'description', header: 'Descrição' },
            { field: 'strategy', header: 'Estratégia' },
            { field: 'valueForStrategy', header: 'Valor para Estratégia' }
        ];

        this.registerForm = this.fb.group( {
            'id': new FormControl(),
            'name': new FormControl( null, Validators.required ),
            'dependsOnLogin': new FormControl( true ),
            'isLoginScreen': new FormControl( false ),
            'description': new FormControl(),
            'pathUrl': new FormControl( null, Validators.required ),
            'system': new FormControl( null, Validators.required ),
            'testUrl': new FormControl(),
            'groupScreenFields': this.fb.array( [ this.createGroupScreenField( 'Grupo Padrão' ) ] )
        } );
        const groupScreenFields = this.registerForm.get( 'groupScreenFields' ) as FormArray;
        groupScreenFields.at( 0 ).patchValue( {
            groupName: 'Grupo Padrão'
        } );

        if ( this.entityEdit ) {
            this.registerForm.setValue( this.entityEdit );
        }
        this.validateFormService.form = this.registerForm;
    }

    createGroupScreenField( name: string ): FormGroup {
        return this.fb.group( {
            'groupName': new FormControl( name, Validators.required ),
            'screenFields': this.fb.array( [ this.createScreenField() ] )

        } );
    }

    addGroupScreen(): void {
        const groupScreenFields = this.registerForm.get( 'groupScreenFields' ) as FormArray;
        groupScreenFields.push( this.createGroupScreenField( 'Clique para editar' ) );
    }

    removeGroupScreen( index: number ): void {
        const groupScreenFields = this.registerForm.get( 'groupScreenFields' ) as FormArray;
        groupScreenFields.removeAt( index );
    }

    createScreenField(): FormGroup {
        return this.fb.group( {
            'description': new FormControl( null, Validators.required ),
            'strategy': new FormControl( null, Validators.required ),
            'valueForStrategy': new FormControl( null, Validators.required ),
        } );
    }

    addscreenField( indexGroupScreen: number ): void {
        const groupScreenFields = this.registerForm.get( 'groupScreenFields' ) as FormArray;
        const screenFields = groupScreenFields.at( indexGroupScreen ).get( 'screenFields' ) as FormArray;
        screenFields.push( this.createScreenField() );
    }

    removeScreenField( indexGroupScreen: number, index: number ): void {
        const groupScreenFields = this.registerForm.get( 'groupScreenFields' ) as FormArray;
        const screenFields = groupScreenFields.at( indexGroupScreen ).get( 'screenFields' ) as FormArray;
        screenFields.removeAt( index );
    }

    ngOnDestroy() {
        this.validateFormService.clear();
    }

    onSubmit( { value, valid }: { value: ScreenSystem, valid: boolean } ) {
        console.log( value, this.registerForm.get( 'groupScreenFields' )[ 'controls' ][ 0 ].get( 'screenFields' )[ 'controls' ] );
        if ( valid ) {
            console.log( value );
            if ( this.entityEdit ) {
                this.msgsService.notifyGrowlEdit( 'Tela' );
                this.editMessageEvent.emit( 'ok' );
            } else {
                this.msgsService.notifyGrowlRegister( 'Tela' );
            }
            this.validateFormService.clear();
        } else {
            this.validateFormService.displayFieldErrors();
        }
    }

    onChangeIsLoginScreen( checked ) {
        if ( checked ) {
            this.registerForm.controls[ 'dependsOnLogin' ].clearValidators();
            this.registerForm.controls[ 'dependsOnLogin' ].updateValueAndValidity();
            this.registerForm.controls[ 'dependsOnLogin' ].reset( { value: false, disabled: true } );
        } else {
            this.registerForm.controls[ 'dependsOnLogin' ].setValidators( Validators.required );
            this.registerForm.controls[ 'dependsOnLogin' ].updateValueAndValidity();
            this.registerForm.controls[ 'dependsOnLogin' ].reset( { value: null, disabled: false } );
        }
    }

    filterSystems( event ) {
        this.filteredSystems = [];
        if ( event.query.toLowerCase() === '' ) {
            this.filteredSystems = [];
        }
        for ( let i = 0; i < this.systems.length; i++ ) {
            if ( this.systems[ i ].name.toLowerCase().indexOf( event.query.toLowerCase() ) === 0 ) {
                this.filteredSystems.push( this.systems[ i ] );
            }
        }
    }

    clearAutoComplete( event ) {
        console.log( this.registerForm.controls );
        this.registerForm.controls[ 'system' ].reset();
    }

    filterStrategies( event ) {
        this.filteredStrategies = [];
        if ( event.query.toLowerCase() === '' ) {
            this.filteredStrategies = [];
        }
        for ( let i = 0; i < this.strategies.length; i++ ) {
            if ( this.strategies[ i ].name.toLowerCase().indexOf( event.query.toLowerCase() ) === 0 ) {
                this.filteredStrategies.push( this.strategies[ i ] );
            }
        }
    }

    clearAutoCompleteStrategy( event ) {
        //        this.registerForm.controls[ 'system' ].reset();
    }

}
