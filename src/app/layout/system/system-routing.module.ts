import { ConsultSystemComponent } from './consult/consult-system.component';
import { RegisterSystemComponent } from './register/register-system.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SystemComponent } from './system.component';

const routes: Routes = [
    {
        path: '', component: SystemComponent,
        children: [
            { path: 'register', component: RegisterSystemComponent },
            { path: 'consult', component: ConsultSystemComponent },

        ]
    }
];

@NgModule( {
    imports: [ RouterModule.forChild( routes ) ],
    exports: [ RouterModule ]
} )
export class SystemRoutingModule { }
