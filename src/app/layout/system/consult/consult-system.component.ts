import { System } from '../../../_models/system';
import { MsgsService } from '../../../_shared/services/msgs.service';
import { routerTransition } from '../../../router.animations';
import { ValidateFormService } from '../../../_shared/services/validate-form.service';
import { SYSTEMS } from '../../../_shared/utils/database-fake';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ConfirmationService } from 'primeng/api';

@Component( {
    selector: 'app-consult-system',
    templateUrl: './consult-system.component.html',
    styleUrls: [ './consult-system.component.scss' ],
    animations: [ routerTransition ],
    providers: [ ConfirmationService, ValidateFormService ]
} )
export class ConsultSystemComponent implements OnInit {

    index = 0;
    consultForm: FormGroup;
    systems: System[];
    cols: any[];
    loading: boolean;
    editTab = false;

    systemEditSelected: System;

    constructor(
        private fb: FormBuilder,
        public validateFormService: ValidateFormService,
        private confirmationService: ConfirmationService,
        private msgsService: MsgsService
    ) { }

    ngOnInit() {
        this.consultForm = this.fb.group( {
            'name': new FormControl(),
        } );
        this.validateFormService.form = this.consultForm;

        this.cols = [
            { field: 'name', header: 'Nome' },
            { field: 'description', header: 'Descrição' },
            { field: 'strategy', header: 'Estratégia Padrão de Captura' }
        ];

        this.systems = SYSTEMS;
    }

    onSubmit( value: string ) {
        console.log( JSON.stringify( value ) );
    }

    handleClose( e ) {
        this.index = 0;
        this.editTab = false;
        e.close();
    }

    handleChange( e ) {
        this.index = e.index;
    }

    receiveEditMessage( $event ) {
        if ( $event ) {
            this.index = 0;
            this.editTab = false;
            this.onSubmit( this.consultForm.value );
        }
    }

    edit( system: System ) {
        this.index = 1;
        this.editTab = true;
        this.systemEditSelected = system;
    }

    remove( system: System ) {
        this.confirmationService.confirm( {
            header: 'Confirmar exclusão',
            message: 'Deseja realmente excluir este registro?',
            icon: 'fa fa-trash',
            accept: () => {
                this.msgsService.notifyGrowlRemove( 'Estretégia' );
            },
            reject: () => {
            }
        } );
    }
}
