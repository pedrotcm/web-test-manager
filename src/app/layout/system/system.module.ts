import { SystemRoutingModule } from './system-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SystemComponent } from './system.component';
import { RegisterSystemComponent } from './register/register-system.component';
import { ConsultSystemComponent } from './consult/consult-system.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { BreadcrumbModule } from '../../_shared/modules/breadcrumb/breadcrumb.module';
import { TabViewModule } from 'primeng/tabview';
import { TooltipModule } from 'primeng/tooltip';
import { ConfirmDialogModule } from 'primeng/confirmdialog';

@NgModule( {
    imports: [
        CommonModule,
        SystemRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        AutoCompleteModule,
        InputTextModule,
        ButtonModule,
        TableModule,
        BreadcrumbModule,
        TabViewModule,
        TooltipModule,
        ConfirmDialogModule
    ],
    declarations: [ SystemComponent, RegisterSystemComponent, ConsultSystemComponent ],
} )
export class SystemModule { }
