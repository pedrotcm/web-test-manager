import { System } from '../../../_models/system';
import { MsgsService } from '../../../_shared/services/msgs.service';
import { routerTransition } from '../../../router.animations';
import { ValidateFormService } from '../../../_shared/services/validate-form.service';
import { getFormValidationErrors, AllValidationErrors } from '../../../_shared/utils/validate-form-errors';
import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';

@Component( {
    selector: 'app-register-system',
    templateUrl: './register-system.component.html',
    styleUrls: [ './register-system.component.scss' ],
    animations: [ routerTransition ],
    providers: [ ValidateFormService ]
} )
export class RegisterSystemComponent implements OnInit, OnDestroy {

    strategies: string[] = [ 'Id', 'xPath', 'Contém Nome' ];
    filteredStrategies: any[];
    registerForm: FormGroup;

    @Input()
    systemEdit: System;

    @Output()
    editMessageEvent = new EventEmitter<string>();


    constructor( private fb: FormBuilder, public validateFormService: ValidateFormService, private msgsService: MsgsService ) { }

    ngOnInit() {
        this.registerForm = this.fb.group( {
            'id': new FormControl(),
            'name': new FormControl( null, Validators.required ),
            'strategy': new FormControl(),
            'description': new FormControl( null, Validators.required )
        } );
        if ( this.systemEdit ) {
            //            this.registerForm.setValue( {
            //                'id': this.systemEdit.id,
            //                'name': this.systemEdit.name,
            //                'strategy': this.systemEdit.strategy,
            //                'description': this.systemEdit.description
            //            } );
            this.registerForm.setValue( this.systemEdit );
        }
        this.validateFormService.form = this.registerForm;
    }

    ngOnDestroy() {
        this.validateFormService.clear();
    }

    filterStrategies( event ) {
        this.filteredStrategies = [];
        if ( event.query.toLowerCase() === '' ) {
            this.filteredStrategies = [];
        }
        for ( let i = 0; i < this.strategies.length; i++ ) {
            if ( this.strategies[ i ].toLowerCase().indexOf( event.query.toLowerCase() ) === 0 ) {
                this.filteredStrategies.push( this.strategies[ i ] );
            }
        }
    }

    clearAutoComplete( event ) {
        this.registerForm.controls[ 'strategy' ].reset();
    }


    onSubmit( { value, valid }: { value: System, valid: boolean } ) {
        if ( valid ) {
            if ( this.systemEdit ) {
                this.msgsService.notifyGrowlEdit( 'Sistema' );
                this.editMessageEvent.emit( 'ok' );
            } else {
                this.msgsService.notifyGrowlRegister( 'Sistema' );
            }
            this.validateFormService.clear();
        } else {
            this.validateFormService.displayFieldErrors();
        }
    }

}
