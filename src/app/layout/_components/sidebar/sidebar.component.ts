import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { animate, style, transition, trigger, state } from '@angular/animations';

@Component( {
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: [ './sidebar.component.scss' ],
    animations: [
        trigger( 'slideInSubmenu', [
            state( 'true', style( {
                overflowY: 'hidden',
            } ) ),
            state( 'false', style( {
                height: 0,
                overflowY: 'hidden'
            } ) ),
            transition( 'true => false', [
                style( { height: '*' } ),
                animate( '0.25s ease-in-out', style( { height: 0 } ) )
            ] ),
            transition( 'false => true', [
                style( { height: '0' } ),
                animate( '0.25s ease-in-out', style( { height: '*' } ) )
            ] )
        ] )
    ]
} )
export class SidebarComponent {
    isActive = false;
    showMenu = '';
    sidebarInactiveClass = 'sidebar-inactive';

    constructor( private translate: TranslateService, public router: Router ) {
        this.translate.addLangs( [ 'en', 'fr', 'ur', 'es', 'it', 'fa', 'de' ] );
        this.translate.setDefaultLang( 'en' );
        const browserLang = this.translate.getBrowserLang();
        this.translate.use( browserLang.match( /en|fr|ur|es|it|fa|de/ ) ? browserLang : 'en' );

        this.router.events.subscribe( val => {
            if ( val instanceof NavigationEnd && window.innerWidth <= 992 && this.isToggled() ) {
                this.toggleSidebar();
            }
        } );
    }

    eventCalled() {
        console.log( 'isActive' + !this.isActive );
        this.isActive = !this.isActive;
    }

    addExpandClass( element: any ) {
        if ( element === this.showMenu ) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector( 'body' );
        return dom.classList.contains( this.sidebarInactiveClass );
    }

    toggleSidebar() {
        const dom: any = document.querySelector( 'body' );
        dom.classList.toggle( this.sidebarInactiveClass );
    }

    rltAndLtr() {
        const dom: any = document.querySelector( 'body' );
        dom.classList.toggle( 'rtl' );
    }

    changeLang( language: string ) {
        this.translate.use( language );
    }

    onLoggedout() {
        localStorage.removeItem( 'isLoggedin' );
    }
}
