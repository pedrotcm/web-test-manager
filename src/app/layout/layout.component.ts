import { Component, OnInit } from '@angular/core';
import { animate, style, transition, trigger, state } from '@angular/animations';
import { Message } from 'primeng/components/common/api';

@Component( {
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: [ './layout.component.scss' ],
    animations: [
        trigger( 'slideInMainContainer', [
            state( 'true', style( {
                marginLeft: '0'
            } ) ),
            transition( 'true => false', [
                style( { marginLeft: '0' } ),
                animate( '0.25s ease-in-out', style( { marginLeft: '*' } ) )
            ] ),
            transition( 'false => true', [
                style( { marginLeft: '*' } ),
                animate( '0.25s ease-in-out', style( { marginLeft: '0' } ) )
            ] )
        ] )
    ]
} )
export class LayoutComponent implements OnInit {

    sidebarInactiveClass = 'sidebar-inactive';

    constructor() { }

    ngOnInit() {
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector( 'body' );
        return dom.classList.contains( this.sidebarInactiveClass );
    }

}
