import { MsgsModule } from '../_shared/modules/messages/msgs.module';
import { MsgsService } from '../_shared/services/msgs.service';
import { ValidateFormService } from '../_shared/services/validate-form.service';
import { LayoutRoutingModule } from './layout-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout.component';
import { HeaderComponent } from './_components/header/header.component';
import { SidebarComponent } from './_components/sidebar/sidebar.component';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule( {
    imports: [
        CommonModule,
        LayoutRoutingModule,
        TranslateModule,
        NgbDropdownModule.forRoot(),
        MsgsModule
    ],
    declarations: [ LayoutComponent, HeaderComponent, SidebarComponent ],
    providers: [ ValidateFormService, MsgsService ]
} )
export class LayoutModule { }
