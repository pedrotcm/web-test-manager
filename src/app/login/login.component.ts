import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component( {
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: [ './login.component.scss' ]
} )
export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    constructor( private router: Router ) { }

    ngOnInit() {
        this.loginForm = new FormGroup( {
            username: new FormControl(),
            password: new FormControl( '', Validators.minLength( 8 ) )
        } );
    }

    get password() {
        return this.loginForm.get( 'password' );
    }

    login(): void {
        console.log( 'navigate to dashboard' );
        this.router.navigate( [ '/dashboard' ] );
    }
}
